#!/bin/bash 
#
# Loaded MJML Compiler - Watcher Cron Task
# Last Updated: 06/02/2019

EMAIL_FOLDERS_PATH=/home/admin/web/loaded.lcprojects.com.au/public_html
WATCH_FILE=/home/admin/mjml-ionotifywait-watch.conf
EXCLUDE_FILE=/home/admin/mjml-ionotifywait-excludes.conf
LOG_FILE=/home/admin/web/loaded.lcprojects.com.au/public_html/__LOGS/loaded-mjml-gulp-compile.log
GULP_BIN=/home/admin/web/loaded.lcprojects.com.au/loaded-mjml-compiler/node_modules/gulp/bin/gulp.js
TIMESTAMP=`date "+%d/%m/%Y %H:%M:%S"`

cd "${0%/*}"

echo "$TIMESTAMP [folders] Scanning for client folders, detected: "
echo "$TIMESTAMP [folders] Scanning for client folders, detected: " >> $LOG_FILE
find $EMAIL_FOLDERS_PATH -maxdepth 1 -mindepth 1 -type d -not -regex "$EMAIL_FOLDERS_PATH/[._].*" -printf '  ->  %P\n'
find $EMAIL_FOLDERS_PATH -maxdepth 1 -mindepth 1 -type d -not -regex "$EMAIL_FOLDERS_PATH/[._].*" -printf '  ->  %P\n' >> $LOG_FILE
find $EMAIL_FOLDERS_PATH -maxdepth 1 -mindepth 1 -type d -not -regex "$EMAIL_FOLDERS_PATH/[._].*" -printf "$EMAIL_FOLDERS_PATH/%P\n" > $WATCH_FILE
cat $EXCLUDE_FILE >> $WATCH_FILE

TIMESTAMP=`date "+%d/%m/%Y %H:%M:%S"`
echo "$TIMESTAMP [inotifywait] Watching email folders \"loaded.lcprojects.com.au\" for changes"
echo "$TIMESTAMP [inotifywait] Watching email folders \"loaded.lcprojects.com.au\" for changes" >> $LOG_FILE

if [ -d "$EMAIL_FOLDERS_PATH" ]; then
  while true; do
    inotifywait -e create,modify --fromfile $WATCH_FILE -r "$EMAIL_FOLDERS_PATH" | 
    while read path action file; do
      if [[ "$file" =~ .*mjml$ ]] ; then
        sleep 1s
        TIMESTAMP=`date "+%d/%m/%Y %H:%M:%S"`
        echo "$TIMESTAMP [inotifywait] Change detected to file \"$path$file\""
        echo "$TIMESTAMP [inotifywait] Change detected to file \"$path$file\"" >> $LOG_FILE
        gulp build --full "$path$file" --fp "$path" --fa "$action" --fn "$file"
        sleep 1s
        TIMESTAMP=`date "+%d/%m/%Y %H:%M:%S"`
        echo "$TIMESTAMP [gulp] Build complete! .html file created in \"$path\""
        echo "$TIMESTAMP [gulp] Build complete! .html file created in \"$path\"" >> $LOG_FILE
        sleep 1s
      fi
    done
  done
fi
