'use strict';

const arg = ( argList => {
  let arg = {},
    a, opt, thisOpt, curOpt;
  for ( a = 0; a < argList.length; a++ ) {
    thisOpt = argList[ a ].trim();
    opt = thisOpt.replace( /^\-+/, '' );
    if ( opt === thisOpt ) {
      // argument value
      if ( curOpt ) arg[ curOpt ] = opt;
      curOpt = null;
    }
    else {
      // argument name
      curOpt = opt;
      arg[ curOpt ] = true;
    }
  }
  return arg;
} )( process.argv );

console.log(arg);

var fullPath = arg['full'];
var fileFolder = arg['fp'];
var fileName = arg['fn'];
var fileAction = arg['fa'];
var htmlFileName = fileName.replace('.mjml', '.html');
var htmlFileOutput = fileFolder + htmlFileName;
console.log(htmlFileOutput);

// import yaml from 'js-yaml';
// import browser from 'browser-sync';
import rimraf from 'rimraf';
import uniqID from 'uniq-id';
import fs from 'fs';
import gulp from 'gulp';
import mjmlGulp from 'gulp-mjml';
import mjml from 'mjml';
// import nunjucks from 'gulp-nunjucks-render';
// import data from 'gulp-data';
import CM_Repeater from "/home/admin/web/loaded.lcprojects.com.au/loaded-mjml-compiler/node_modules/mjml-campaignmonitor/lib/Repeater.js"
import CM_Layout from "/home/admin/web/loaded.lcprojects.com.au/loaded-mjml-compiler/node_modules/mjml-campaignmonitor/lib/Layout.js"
import CM_Image from "/home/admin/web/loaded.lcprojects.com.au/loaded-mjml-compiler/node_modules/mjml-campaignmonitor/lib/Image.js"
import babel from 'gulp-babel'
import rename from 'gulp-rename'
import gutil from 'gulp-util'
import path from 'path'
import { exec } from 'child_process'
import mjml2html from 'mjml'
import { registerComponent } from 'mjml-core'

registerComponent( CM_Repeater )
registerComponent( CM_Layout )
registerComponent( CM_Image )

fs.access(htmlFileOutput, (err) => {
  if (err) {
      console.log('HTML Output file does not already exist, will create new');        
  } else {
      console.log('HTML Output file exists, moving to Trash Folder');
      var newUUID = uniqID.generateUUID('xxxxyxxxxyxxxxyxxxxy', 16)();
      exec('mv "'+htmlFileOutput+'" "/home/admin/web/loaded.lcprojects.com.au/trash/'+newUUID+'.'+htmlFileName+'"' , function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
      });
  }
});

export function buildMjml() {
  const options = {
    beautify: true,
    minify: false
  };
  return gulp.src(fullPath).pipe( mjmlGulp( mjml, options ) )
  .pipe(rename(htmlFileName))
  .pipe( gulp.dest( fileFolder ) );
}

gulp.task( 'build', gulp.series( buildMjml ) );
gulp.task( 'default', gulp.series( buildMjml ) );
