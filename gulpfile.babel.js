'use strict';

const arg = ( argList => {
  let arg = {},
    a, opt, thisOpt, curOpt;
  for ( a = 0; a < argList.length; a++ ) {
    thisOpt = argList[ a ].trim();
    opt = thisOpt.replace( /^\-+/, '' );
    if ( opt === thisOpt ) {
      // argument value
      if ( curOpt ) arg[ curOpt ] = opt;
      curOpt = null;
    }
    else {
      // argument name
      curOpt = opt;
      arg[ curOpt ] = true;
    }
  }
  return arg;
} )( process.argv );

// console.log(arg);
if (Array.isArray(arg)) {
  var fullPath = arg['full'];
  var fileFolder = arg['fp'];
  var fileName = arg['fn'];
  var fileAction = arg['fa'];
  var htmlFileName = fileName.replace('.mjml', '.html');
  var htmlFileOutput = fileFolder + htmlFileName;
} else {
  var fullPath = "/home/admin/web/loaded.lcprojects.com.au/public_html/new-client/campaign-redesign-2019/new-name.mjml";
  var fileFolder = "/home/admin/web/loaded.lcprojects.com.au/public_html/new-client/campaign-redesign-2019/";
  var fileName = "new-name.mjml";
  var fileAction = "MODIFY ";
  var htmlFileName = "new-name.html";
  var htmlFileOutput = "/home/admin/web/loaded.lcprojects.com.au/public_html/new-client/campaign-redesign-2019/new-name.html";
}

console.log(htmlFileOutput);

// import yaml from 'js-yaml';
// import browser from 'browser-sync';
import rimraf from 'rimraf';
import uniqID from 'uniq-id';
import fs from 'fs';
import gulp from 'gulp';
import mjmlGulp from 'gulp-mjml';
import mjml from 'mjml';
// import nunjucks from 'gulp-nunjucks-render';
// import data from 'gulp-data';
import babel from 'gulp-babel'
import rename from 'gulp-rename'
import gutil from 'gulp-util'
import path from 'path'
import exec from 'child_process'
import mjml2html from 'mjml'
import registerComponent from 'mjml'
// import { SendEmail } from 'aws-mjml-csv'
// import * as AWS from 'aws-sdk'

var CM_Repeater = "/home/admin/web/loaded.lcprojects.com.au/loaded-mjml-compiler/node_modules/mjml-campaignmonitor/lib/Repeater.js"
var CM_Layout = "/home/admin/web/loaded.lcprojects.com.au/loaded-mjml-compiler/node_modules/mjml-campaignmonitor/lib/Layout.js"
var CM_Image = "/home/admin/web/loaded.lcprojects.com.au/loaded-mjml-compiler/node_modules/mjml-campaignmonitor/lib/Image.js"

registerComponent( CM_Repeater )
registerComponent( CM_Layout )
registerComponent( CM_Image )

fs.access(htmlFileOutput, (err) => {
  if (err) {
      console.log('HTML Output file does not already exist, will create new');        
  } else {
      console.log('HTML Output file exists, moving to Trash Folder');
      var newUUID = uniqID.generateUUID('xxxxyxxxxyxxxxyxxxxy', 16)();
      exec('mv "'+htmlFileOutput+'" "/home/admin/web/loaded.lcprojects.com.au/trash/'+newUUID+'.'+htmlFileName+'"' , function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
      });
  }
});

// const instance = new SendEmail({
//   source: 'tyson@loadedcommunications.com.au'
// })

// (async () => {
//   await instance.setMjml(fullPath)
//   // await instance.setCsvfromPath('/home/admin/web/loaded.lcprojects.com.au/loaded-mjml-compiler/test/data.csv')
//   let sent = 0
//   let errors = 0
//   instance.on('sent', ({ result, row, time }) => {
//     sent++
//   })
//   instance.on('error', ({ error, row, time }) => {
//     errors++
//   })
//   await instance.send()
//   console.log(`Sent ${sent} emails (${error} failed)`)
// })()

const options = {
  beautify: true,
  minify: false
};

gulp.task('compile', function () {
  return  gulp.src(fullPath)
              .pipe( mjmlGulp( mjml, options ) )
              .pipe(gulp.dest(fileFolder));
});

gulp.task( 'default', ['compile'] );
